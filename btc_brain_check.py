import sys
from gen_btc import checkAddress as checkBtcAddress, generateBrainwalletAddress as generateBrainwalletBtcAddress
from save import DB

from multiprocessing import Pool


db = DB()

def check(password):
    try:
        addr = generateBrainwalletBtcAddress(password)
        qtty = checkBtcAddress(addr.get('address'))
        if qtty > 0:
            print("%s:%d (%s)" % (addr.get('address'), qtty, password))
            db.save(addr)
    except:
        pass

if len(sys.argv) > 1:
    with open(sys.argv[1]) as f:
        passwords =  f.read().split('\n')
else:
    pass_aux = input('Passwords comma separated (or nothing for incremental) > ')
    if len(pass_aux) > 0:
        passwords = pass_aux.split(',')
    else:
        min, max = input("min,max > ").split(',')
        verbose = True
        passwords = [str(x) for x in range(int(max))[int(min):]]

# while len(passwords) > 0:
p = Pool(8)
p.map(check, passwords)
