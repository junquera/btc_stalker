NAME=btcStalker
if [ $# -eq 1 ]; then
	HILOS=$1
else
	HILOS=4
fi
COUNT=
tmux start-server\; new-window -a -n $NAME
tmux new-session -s $NAME -d python3 gen_btc.py $HILOS
tmux split-window -h -d htop
tmux selectp -t $NAME
tmux split-window -v -d tail -f wallets.txt
tmux set-window-option -g window-status-current-bg blue
tmux bind-key k kill-session -t $NAME
tmux attach-session -t $NAME
#touch wallets.txt &&
#tmux splitw -d tail -f wallets.txt &&
#python3 gen_btc.py >> wallets.txt
