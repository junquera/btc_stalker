#!/usr/bin/env python3
import hashlib
import base58
from ecdsa import SigningKey, SECP256k1

import requests
import sys
import re

from db import DB

def generateHash(i):
  sk = SigningKey.from_secret_exponent(i,curve=SECP256k1)
  public = sk.privkey.public_key
  barr = "04%x%x"%(public.point.x(), public.point.y())
  pk=bytearray.fromhex(barr)
  hash_key = "00" + hashlib.new('ripemd160', hashlib.sha256(pk).digest()).hexdigest()

  return bytearray.fromhex(hash_key)

def generateAddress(privateKey):
    hash_key=generateHash(privateKey)
    checksum = hashlib.sha256(hashlib.sha256(hash_key).digest()).digest()
    address=base58.b58encode(b''.join([hash_key, checksum[:4]]))

    return address.decode('utf-8')


import sha3
# https://kobl.one/blog/create-full-ethereum-keypair-and-address/
def generateEthereumAddress(privateKey):
    sk = SigningKey.from_secret_exponent(privateKey, curve=SECP256k1)

    public = sk.privkey.public_key


    barr = "%x%x"%(public.point.x(), public.point.y())

    pk=bytearray.fromhex(barr)
    k = sha3.keccak_256()
    k.update(pk)

    hash_key = "0x" + k.hexdigest()[-40:]

    return hash_key

def generateRandomAddress():
    sk = SigningKey.generate(curve=SECP256k1)
    sec_exp = sk.privkey.secret_multiplier

    try:
        address = generateAddress(sec_exp)
    except Exception as e:
        # print(e)
        raise Exception("Error al generar direccion")

    return {'address': address, 'key': sec_exp}

def generateRandomEthereumAddress():
    sk = SigningKey.generate(curve=SECP256k1)
    sec_exp = sk.privkey.secret_multiplier

    try:
        address = generateEthereumAddress(sec_exp)
    except Exception as e:
        # print(e)
        raise Exception("Error al generar direccion")

    return {'address': address, 'key': sec_exp}


def generateBrainwalletAddress(password):
    generator = int(hashlib.sha256(password.encode('utf-8')).hexdigest(), 16)

    sk = SigningKey.from_secret_exponent(generator, curve=SECP256k1)

    sec_exp = sk.privkey.secret_multiplier

    try:
        address = generateAddress(sec_exp)
    except Exception as e:
        # print(e)
        raise Exception("Error al generar direccion")

    return {'address': address, 'key': sec_exp}

def generateBrainwalletEthereumAddress(password):

    generator = int(hashlib.sha256(password.encode('utf-8')).hexdigest(), 16)

    sk = SigningKey.from_secret_exponent(generator, curve=SECP256k1)

    sec_exp = sk.privkey.secret_multiplier

    try:
        address = generateEthereumAddress(sec_exp)
    except Exception as e:
        raise Exception("Error al generar direccion [generateBrainwalletEthereumAddress]")

    return {'address': address, 'key': sec_exp}

def checkAddress(address, verbose=False):
    r = requests.get('https://blockchain.info/es/q/addressbalance/%s' % address)

    if r.status_code == 200:
        quantity = int(r.content)
    else:
        print("[x] Error %d:\n%s" (r.status_code, r.text))
        quantity = 0

    if verbose:
        print("[%d] %s:%d" % (r.status_code, address, quantity))

    return quantity



def checkEthAddress(address, verbose=False):

    r = requests.get('https://api.etherscan.io/api?module=account&action=balance&address=%s' % address)

    if r.status_code == 200:
        quantity = int(r.json().get('result'))
    else:
        quantity = 0

    if verbose:
        print("[%d] %s:%d" % (r.status_code, address, quantity))

    return quantity

import multiprocessing as mp
import time

def gen_and_check_async():
    a = time.time()
    try:
        address = generateRandomAddress()
        generation.put(time.time() - a)
    except Exception as e:
        # print(e)
        return
    b = time.time()
    quantity = checkAddress(address['address'])
    internet.put(time.time() - b)
    if quantity > 0:
        address['quantity'] = quantity
        output.put(address)

def gen_pattern_and_check_async():
    a = time.time()
    try:
        address = generateRandomAddress()
        generation.put(time.time() - a)
    except Exception as e:
        # print(e)
        return

    if len(pattern) > 0:
        if re.match(r'1%s' % pattern, address['address']) is None:
            internet.put(0)
            return

        sys.stderr.writelines("[!] With pattern: %s\n" % (address['address']))

    b = time.time()
    quantity = checkAddress(address['address'])
    internet.put(time.time() - b)
    if quantity > 0:
        address['quantity'] = quantity
        output.put(address)


from functools import reduce

pattern=""

output = mp.Queue()

generation = mp.Queue()
internet = mp.Queue()

def main(pool_size):

    db = DB()
    print("Starting with a pool of %d threads" % pool_size)

    it = 0

    tinit = time.time()
    itinit = 0

    while True:

        processes = [mp.Process(target=gen_and_check_async) for result in range(pool_size)]

        for p in processes:
            p.start()

        for p in processes:
            p.join()

        while not output.empty():
            a = output.get()
            try:
                db.save(a)
                with open('%s.txt' % a.get('address'), 'w+') as f:
                    f.write(str(a))
            except:
                pass

        it += pool_size


        inter = []
        while not internet.empty():
            inter.append(internet.get())

        gener = []
        while not generation.empty():
            gener.append(generation.get())

        g = reduce((lambda x, y: ((x + y) / 2)), gener) if len(gener) > 0 else 0
        i = reduce((lambda x, y: ((x + y) / 2)), inter) if len(inter) > 0 else 0
        # sys.stderr.writelines("[*] Iter: %d;\tGeneration: %f; Internet: %f;\n" % (pool_size * it, g, i))
        generation.put(g)
        internet.put(i)

        if time.time() - tinit > 10:
            nops = it - itinit
            sys.stderr.writelines("[i] %.02f ops/s\n" % (nops / 10.))
            tinit = time.time()
            itinit = it

if __name__ == '__main__':


    pool_size = 4
    # TODO Add arg parser
    if len(sys.argv) > 1:
        pool_size = int(sys.argv[1])

    main(pool_size)
