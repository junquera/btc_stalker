import sqlite3

class DB():

    def __init__(self, name='btc_stalker'):
        self.conn = sqlite3.connect("%s.db" % name)
        try:
            c = self.conn.cursor()

            c.execute('''CREATE TABLE wallets
                        (address text, key text)''')

            self.conn.commit()
        except:
            pass

    def save(address):
        try:
            c = self.conn.cursor()

            c.execute('''INSERT INTO wallets VALUES
                        ("%s", "%s")''' % (address.get('address'), address.get('key')))

            self.conn.commit()
            print("Saved %s" % address.get('address'))
        except:
            print("[!] ERROR SAVING:")
            print(address)
