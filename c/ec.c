/* ------------------------------------------------------------ *
 * file:        eckeycreate.c                                   *
 * purpose:     Example code for creating elliptic curve        *
 *              cryptography (ECC) key pairs                    *
 * author:      01/26/2015 Frank4DD                             *
 *                                                              *
 * gcc -o eckeycreate eckeycreate.c -lssl -lcrypto              *
 * ------------------------------------------------------------ */


/**
  sk = SigningKey.from_secret_exponent(i,curve=SECP256k1)
  public = sk.privkey.public_key
  barr = "04%x%x"%(public.point.x(), public.point.y())
  pk=bytearray.fromhex(barr)
  hash_key = "00" + hashlib.new('ripemd160', hashlib.sha256(pk).digest()).hexdigest()
  checksum = hashlib.sha256(hashlib.sha256(hash_key).digest()).digest()
  address=base58.b58encode(b''.join([hash_key, checksum[:4]]))

**/
#include <stdio.h>

#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/ec.h>
#include <openssl/pem.h>
#include <openssl/bn.h>

#define ECCTYPE    "secp256k1"

struct btc_wallet {
  EVP_PKEY *pkey;
  char *address;
};

int main() {

  EC_KEY            *myecc  = NULL;
  EVP_PKEY          *pkey   = NULL;
  int               eccgrp;

  /* ---------------------------------------------------------- *
   * These function calls initialize openssl for correct work.  *
   * ---------------------------------------------------------- */
  OpenSSL_add_all_algorithms();

  /* ---------------------------------------------------------- *
   * Create a EC key sructure, setting the group type from NID  *
   * ---------------------------------------------------------- */
  eccgrp = OBJ_txt2nid(ECCTYPE);
  myecc = EC_KEY_new_by_curve_name(eccgrp);

  /* -------------------------------------------------------- *
   * For cert signing, we use  the OPENSSL_EC_NAMED_CURVE flag*
   * ---------------------------------------------------------*/
  EC_KEY_set_asn1_flag(myecc, OPENSSL_EC_NAMED_CURVE);

  /* -------------------------------------------------------- *
   * Create the public/private EC key pair here               *
   * ---------------------------------------------------------*/
  EC_KEY_generate_key(myecc);

  /* -------------------------------------------------------- *
   * Converting the EC key into a PKEY structure let us       *
   * handle the key just like any other key pair.             *
   * ---------------------------------------------------------*/
  pkey=EVP_PKEY_new();
  EVP_PKEY_assign_EC_KEY(pkey,myecc);

  /* -------------------------------------------------------- *
   * Now we show how to extract EC-specifics from the key     *
   * ---------------------------------------------------------*/
  myecc = EVP_PKEY_get1_EC_KEY(pkey);
  EC_GROUP *ecgrp = EC_KEY_get0_group(myecc);

  /* ---------------------------------------------------------- *
   * Here we print the private/public key data in PEM format.   *
   * ---------------------------------------------------------- */
  EC_POINT *public = EC_KEY_get0_public_key(myecc);

  BIGNUM *x = BN_new();
  BIGNUM *y = BN_new();

  EC_POINT_get_affine_coordinates_GFp(ecgrp, public, x, y, NULL);

  char *x_hex = BN_bn2hex(x);
  char *y_hex = BN_bn2hex(y);

  BN_free(x);
  BN_free(y);

  char *barr = malloc(sizeof(char) * 122);
  // https://wiki.openssl.org/index.php/Manual:BN_bn2bin(3)
  sprintf(barr, "04%x%x", x, y); // Hex value

  printf("%s", barr);

  struct btc_wallet wallet;
  wallet.address = barr;
  /* ---------------------------------------------------------- *
   * Free up all structures                                     *
   * ---------------------------------------------------------- */
  EVP_PKEY_free(pkey);
  EC_KEY_free(myecc);

}
